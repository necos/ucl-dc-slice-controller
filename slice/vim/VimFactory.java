package slice.vim;

import java.util.List;
import java.lang.reflect.Constructor;

import slice.resource.Host;
import slice.resource.HostPort;
import us.monoid.json.JSONObject;

/**
 * A singleton
 */
public class VimFactory {
    private final static VimFactory factory = new VimFactory();
    
    /**
     * Try and instantiate a VIM of a particular type over a given set of hosts.
     *
     * The class name of the VimInitiator is created from the type of the Vim.
     * e.g type = 'vlsp' --- 'slice.vim.VlspInitiator'
     * or  type = ''
     */
    public static VimInfo instantiate(String type, HostPort vimHost, List<Host> allocation, String remoteUser, JSONObject remoteConfig) {
        // create class name
        String className = "slice.vim." + factory.capitalize(type) + "Initiator";

        System.err.println("Vim class name = " + className);

        // create a VimInitiator
        VimInitiator initiator = null;

        if ((initiator = factory.setupVimInitiator(className)) != null) {

            // Now get the VimInitiator to instantiate w.r.t the args passed in
            return initiator.instantiate(type, vimHost, allocation, remoteUser, remoteConfig);
        } else {

            System.err.println("Cannot find Class for Vim class name = " + className);
            return null;
        }
    }
    
   /**
     * Set up a VimInitiator
     */
    private VimInitiator setupVimInitiator(String vimInitiatorClassName) {
        try {
            VimInitiator newVimInitiator = null;
            
            Class<?> c = Class.forName(vimInitiatorClassName);
            Class<? extends VimInitiator> cc = c.asSubclass(VimInitiator.class);

            // find Constructor for when no arg
            Constructor<? extends VimInitiator> cons = cc.getDeclaredConstructor();

            newVimInitiator = cons.newInstance();

            System.out.println(leadin() + "Setup VimInitiator: " + newVimInitiator);

            // if we get here we instantiated the VimInitiator OK
            // so set it for further use
            return newVimInitiator;

        } catch (ClassNotFoundException cnfe) {
            System.err.println(leadin() + "Class not found " + vimInitiatorClassName);
            return null;
        } catch (Exception e) {
            System.err.println(leadin() + "Cannot instantiate class " + vimInitiatorClassName);
            e.printStackTrace();
            return null;
        }
    }


    private String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    private String leadin() {
        return "ViolationProcessor: ";
    }

}
