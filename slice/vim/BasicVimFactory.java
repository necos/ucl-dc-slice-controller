package slice.vim;

import java.util.List;

import slice.resource.Host;
import slice.resource.HostPort;
import us.monoid.json.JSONObject;

/**
 * A singleton
 */
public class BasicVimFactory {
    /**
     * Try and instantiate a VIM of a particular type
     * over a given set of hosts
     */
    public static VimInfo instantiate(String type, HostPort vimHost, List<Host> allocation, String remoteUser, JSONObject remoteConfig) {

        if (type.equals("vlsp")) {
            // try the VlspInitiator
            VlspInitiator vlsp = new VlspInitiator();
            return vlsp.instantiate(type, vimHost, allocation, remoteUser, remoteConfig);
        } else {
            return null;
        }
    }

}
