package slice.vim;

import java.util.Map;
import java.io.IOException;

/**
 * A template engine
 */
public interface TemplateEngine {
        /**
     * Generate a file froma template and some info.
     * Returns the Generated filename.
     */
    public String generate(String templatePath, Map<String, Object> info, String dstPath, String dstFileName) throws IOException;


}
