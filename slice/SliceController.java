package slice;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import us.monoid.json.JSONObject;
import us.monoid.json.JSONException;

import slice.slice.*;
import slice.console.*;
import slice.resource.*;
import slice.vim.*;

import cc.clayman.logging.*;

public class SliceController {
    // The config file name
    String configFileName;

    // ResourceManagager
    ResourceManager resources;

    // User manager
    UserManager userManager;

    // The console
    SliceControllerConsole console;

    // A Store for Slice info
    SliceStore sliceStore;
    
    // The port for the ManagementConsole
    int consolePort = 7080;


    JSONObject config;



    /**
     * Main entry point.
     */
    public static void main(String[] args) {
        SliceController sc = new SliceController();

        boolean initOK = false;

        initOK = sc.init(args);

        // logging setup by now

        if (initOK) {
            boolean startOK = sc.start();

            if (!startOK) {
                System.exit(1);
            }
        }

    }

    /**
     * Construct a SliceController
     */
    public SliceController() {
        config = new JSONObject();
    }

    /** 
     *  Initialisation for the SliceController
     */
    public boolean init(String [] args) {
        // allocate a new logger
        Logger logger = Logger.getLogger("log");

        // tell it to output to stdout and tell it what to pick up
        // it will actually output things where the log has bit
        // MASK.STDOUT set

        // tell it to output to stderr and tell it what to pick up
        // it will actually output things where the log has bit
        // MASK.ERROR set
        logger.addOutput(System.err, new BitMask(MASK.ERROR));
        logger.addOutput(System.out, new BitMask(MASK.STDOUT));

        // add some extra output channels, using mask bit 7, 8, 9, 10
        try {
            //logger.addOutput(new PrintWriter(new FileOutputStream("/tmp/slicec-channel7.out")), new BitMask(1<<9));
            //logger.addOutput(new PrintWriter(new FileOutputStream("/tmp/slicec-channel8.out")), new BitMask(1<<9));
            //logger.addOutput(new PrintWriter(new FileOutputStream("/tmp/slicec-channel9.out")), new BitMask(1<<9));
            //logger.addOutput(new PrintWriter(new FileOutputStream("/tmp/slicec-channel10.out")), new BitMask(1<<10));
            //logger.addOutput(new PrintWriter(new FileOutputStream("/tmp/slicec-channel11.out")), new BitMask(1<<11));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


        // Process the args

        // Get config file name
        if (args.length == 1) {
            configFileName = args[0];
        } else {
            configFileName = "config/basic.json";
        }


        try {
            String configStr = readConfigFile(configFileName);
            config = new JSONObject(configStr);

        } catch (IOException e) {
            Logger.getLogger("log").logln(MASK.ERROR, leadin() + "Cannot open config file: " + configFileName);
            return false;
        } catch (JSONException je) {
            Logger.getLogger("log").logln(MASK.ERROR, leadin() + "Invalid json in config file: " + configFileName + " at " + je.getMessage());
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        // Start the Resource Manager
        // It keeps track of all the hosts        
        try {
            // It takes the "spaces" element
            resources = new IncoreResourceManager(config.getJSONObject("spaces"));
        } catch (JSONException je) {
            Logger.getLogger("log").logln(MASK.ERROR, leadin() + "Config error: " + je.getMessage());
            return false;
        } catch (Exception e) {
            Logger.getLogger("log").logln(MASK.ERROR, leadin() + "Cannot start ResourceManager with config: " + config);
            return false;
        }


        // Start the User Manager
        // it keeps track of all the remote users on those hosts
        try {
            // It takes the "user" element
            userManager = new BasicUserManager(config.optJSONObject("user"));
        } catch (Exception e) {
            Logger.getLogger("log").logln(MASK.ERROR, leadin() + "Cannot start UserManager with config: " + config);
            return false;
        }


        sliceStore = new HashMapSliceStore();

        return true;
    }

    /**
     * Start the SliceController
     */
    public boolean start() {
        boolean resVal = resources.start();
        
        if (resVal) {

            startConsole();

            Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Started");

            return true;
        } else {

            return false;
        }

    }

    /**
     * Stop the SliceController.
     */
    public boolean stop() {
        Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Stopping");

        // Stop resources
        stopConsole();

        // close all processes
        for (Slice slice : getSlices()) {
            Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Release Slice " + slice);
            releaseSlice(slice);
        }

        // Stop resources
        Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Stop ResourceManager ");
        boolean resVal = resources.stop();

        Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Stopped");

        return true;
    }

    /**
     * Shutdown
     */
    public void shutDown() {
        stop();
    }
    /**
     * Start the console.
     */
    protected void startConsole() {
        console = new SliceControllerConsole(this, getPort());
        console.start();
    }

    /**
     * Stop the console.
     */
    protected void stopConsole() {
        console.stop();
    }


    /**
     * Get the port for the ManagementConsole
     */
    public int getPort() {
        return consolePort;
    }


    /**
     * Read a config file and return it as a string
     */
    protected String readConfigFile(String filename) throws IOException {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            throw e;
        }        

        String line = null;
        StringBuilder stringBuilder = new StringBuilder();

        System.getProperty("line.separator");

        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(" ");
        }
        reader.close();

        return stringBuilder.toString();

    }

    /* Main functional methods */

    /**
     * Is this arg a valid slice ID
     */
    public boolean isValidSliceID(int id) {
        return ! (sliceStore.get(id) == null);
    }

    /**
     * Get a list of the slices
     */
    public List<Slice> getSlices() {
        Set<Integer> sliceIDs = sliceStore.getSliceIDs();

        List<Slice> slices = new ArrayList<Slice>();

        for (int id : sliceIDs) {
            slices.add(sliceStore.get(id));
        }

        return slices;
    }


    /**
     * Register a new slice 
     */
    public int registerSlice(Slice slice) {
        int id = sliceStore.add(slice);

        Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Registered: " + slice + " id: " + slice.getID());

        return sliceStore.count();
    }

    /**
     * Release a slice
     */
    public boolean releaseSlice(int id) {
        Slice slice = sliceStore.get(id);
        return releaseSlice(slice);
    }
    
    public boolean releaseSlice(Slice slice) {
        // Stop the VIM process
        VimInfo info = slice.getVimInfo();

        Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "about to releaseSlice: " + slice);

        ProcessWrapper processWrapper = info.processWrapper;

        processWrapper.stop();

        Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "releaseSlice: process stopped");


        // Free up the resources
        resources.free(slice.getAllocation());

        // Remove slice
        sliceStore.remove(slice.getID());

        return true;
    }
    
    /**
     * Get the ResourceManager
     */
    public ResourceManager getResourceManager() {
        return resources;
    }


    /**
     * Get the UserManager
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * Get the config details for remote hosts.
     * This is the "remote" element of the config
     */ 
    public JSONObject getRemoteConfig() {
        return config.optJSONObject("remote");
    }
    
    /**
     * Leadin String for msgs
     */
    protected String leadin() {
        return "Slice Ctrl: ";
    }
}
