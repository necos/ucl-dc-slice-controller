package slice.slice;

import java.util.List;
import slice.resource.Host;
import slice.resource.HostPort;
import slice.vim.VimInfo;


public interface Slice {
    /**
     * Get the size of the slice.
     */
    public int getSize();

    /**
     * Get the type of the slice.
     */
    public String getType();

    /**
     * Get the Host and Port of the vim
     */
    public HostPort getVimHost();


    /**
     * Get the hosts allocated for the slice
     */
    public List<Host> getAllocation();


    /**
     * Get the Info for the deployed VIM
     */
    public VimInfo getVimInfo();

    /**
     * Get the slice ID
     */
    public int getID();

}
