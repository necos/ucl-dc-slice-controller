package slice.slice;

import java.util.HashMap;
import java.util.Set;

public class HashMapSliceStore implements SliceStore {
    HashMap<Integer,Slice> map;
    
    public HashMapSliceStore() {
        map = new HashMap<Integer,Slice>();
    }
    
    /**
     * Add a slice
     * Returns a unique code
     */
    public int add(Slice slice) {
        int id = slice.getID();

        map.put(id, slice);
        return id;
    }

    /**
     * Get a slice by ID
     * Returns the slice
     */
    public Slice get(int sliceID) {
        return map.get(sliceID);
    }

    /**
     * Remove a slice
     * Returns the slice
     */
    public Slice remove(Slice s) {
        return map.remove(s.getID());
    }

    /**
     * Remove a slice by ID
     * Returns the slice
     */
    public Slice remove(int sliceID) {
        Slice removed =  map.remove(sliceID);

        return removed;
    }


    /**
     * Count the number of slices
     */
    public int count() {
        return map.size();
    }


    /** 
     * Get a set of all the slice IDs
     */
    public Set<Integer>	getSliceIDs() {
        return map.keySet();
    }
}
