package slice.resource;

import java.io.IOException;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;


public class  BasicUserManager implements UserManager {
    JSONObject jsConfig = null;

    public BasicUserManager(JSONObject confVal) throws IOException {
        jsConfig = confVal;
    }
    
    /**
     * This finds the name of the remote user
     * Options are:
     * 1.  from config
     * 2.  from env
     * 3.  from auth module.
     */
    public String findRemoteUser() {
        String username = null;

        // Find from config
        // Get the field "username"
        if (jsConfig != null) {
            username = jsConfig.optString("username");
        }

        // Find from env
        if (username == null) {
            username = System.getProperty("user.name");
        }

        // Find from auth module
        if (username == null) {
            username = new com.sun.security.auth.module.UnixSystem().getUsername();
        }

        System.err.println("BasicUserManager: findRemoteUser = " + username);

        return username;

    }
}
