package slice.resource;

import java.util.List;
import java.util.ArrayList;

import java.util.Random;
    
/**
 * The Placement Manager for VIMs
 * It takes a set of hosts and works out where to place a VIM
 */
public class VimPlacementManager {
    // The host list for vims.
    // This is used for placing VIMs
    List<Host> slice0;   // set in config

    // A Random number generator
    Random generator;

    public VimPlacementManager() {
        slice0 = new ArrayList<Host>();
        generator = new Random(System.currentTimeMillis());
    }


    /**
     * Set the hosts to use for this manager
     */
    public int setHosts(List<Host> hosts) {
        for (Host h : hosts) {
            if (! slice0.contains(h)) {
                // host h not in slice0
                slice0.add(h);

                System.err.println("VimPlacementManager: add host " + h);
            }
        }

        return slice0.size();
    }


    /**
     * Find host to place a VIM for managing sliceSize hosts
     */
    public Host findHost(int sliceSize, String sliceType) {
        // return a random one
        int random = generator.nextInt(slice0.size());

        Host h = slice0.get(random);

        return h;
    }
}
