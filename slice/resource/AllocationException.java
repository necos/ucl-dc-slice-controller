package slice.resource;

public class AllocationException extends Exception {
    public AllocationException() {
        super();
    }


    public AllocationException(String s) {
        super(s);
    }
}
