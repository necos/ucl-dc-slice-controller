package slice.resource;

import java.util.ArrayList;

import java.util.concurrent.Semaphore;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;


public abstract class AbstractResourceManager implements ResourceManager {

    // A default config
    static final String DEFAULT_CONFIG = "{\"spaces\":{\"slice0\":[\"localhost\"],\"hosts\":[\"localhost\"]}}";

    // A Semaphore for single access
    Semaphore semaphore = new Semaphore(1);


    // A Semaphore timeout in millis
    static final int semaphoreTimeout = 2000;


    protected ArrayList cvtToArrayList(JSONArray arr) {
        ArrayList list = new ArrayList();

        for (int i = 0; i < arr.length(); i += 1) {
            list.add(arr.opt(i));
        }

        return list;
    }

}
