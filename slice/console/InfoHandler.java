package slice.console;

import slice.slice.*;
import slice.resource.AllocationException;

import org.simpleframework.http.Response;
import org.simpleframework.http.Request;
import org.simpleframework.http.Path;
import org.simpleframework.http.Query;
import us.monoid.json.*;

import java.util.Scanner;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.io.PrintStream;
import java.io.IOException;

import cc.clayman.console.*;

import cc.clayman.logging.*;

/**
 * A class to handle /info/ requests
 */
public class InfoHandler extends BasicRequestHandler implements RequestHandler {
    SliceControllerConsole console;


    public InfoHandler() {
        // allocate a new logger
        Logger logger = Logger.getLogger("log");

        logger.addOutput(System.err, new BitMask(MASK.ERROR));
        logger.addOutput(System.out, new BitMask(MASK.STDOUT));

    }


    /**
     * Handle a request and send a response.
     */
    public boolean  handle(Request request, Response response) {
        // get RestListener
        console = (SliceControllerConsole)getManagementConsole();

        try {
            /*
            System.out.println("method: " + request.getMethod());
            System.out.println("target: " + request.getTarget());
            System.out.println("path: " + request.getPath());
            System.out.println("directory: " + request.getPath().getDirectory());
            System.out.println("name: " + request.getPath().getName());
            System.out.println("segments: " + java.util.Arrays.asList(request.getPath().getSegments()));
            System.out.println("query: " + request.getQuery());
            System.out.println("keys: " + request.getQuery().keySet());
            */

            //System.out.println("\n/slice/ REQUEST: " + request.getMethod() + " " +  request.getTarget());

            long time = System.currentTimeMillis();

            response.set("Content-Type", "application/json");
            response.set("Server", "Slice Controller/1.0 (SimpleFramework 4.0)");
            response.setDate("Date", time);
            response.setDate("Last-Modified", time);

            // get the path
            Path path =  request.getPath();
            String directory = path.getDirectory();
            String name = path.getName();
            String[] segments = path.getSegments();

            // Get the method
            String method = request.getMethod();

            // Get the Query
            Query query = request.getQuery();


            // and evaluate the input
            if (method.equals("POST")) {
                notFound(response, "POST not supported");

                
            } else if (method.equals("DELETE")) {
                notFound(response, "DELETE not supported");
                
            } else if (method.equals("GET")) {
                if (name == null) {      // no arg, so get data
                    getInfo(request, response);
                } else if (segments.length == 2) {   // get specific  info
                } else {
                    notFound(response, "GET bad request");
                }
                
            } else if (method.equals("PUT")) {
                notFound(response, "PUT not supported");
                
            } else {
                badRequest(response, "Unknown method" + method);
            }



            // check if the response is closed
            response.close();

            return true;

        } catch (IOException ioe) {
            System.err.println("IOException " + ioe.getMessage());
        } catch (JSONException jse) {
            System.err.println("JSONException " + jse.getMessage());
        }

        return false;

    }

    /**
     * GET: Get some data given a request and send a response.
     */
    public void getInfo(Request request, Response response) throws IOException, JSONException  {
        Logger.getLogger("log").logln(MASK.STDOUT, "REST Info Handler: " +  "getInfo");

        // process query

        Query query = request.getQuery();

        // and send them back as the return value
        PrintStream out = response.getPrintStream();

        JSONObject jsobj = new JSONObject();

        jsobj.put("message", "info");
        jsobj.put("timestamp", System.currentTimeMillis());

        JSONObject payload = new JSONObject();

        // location
        JSONObject location = new JSONObject();

        location.put("city", "London");
        location.put("country", "UK");

        payload.put("location", location);

        // vim types
        JSONArray types = new JSONArray();
        types.put("vlsp");

        payload.put("types", types);

        jsobj.put("payload", payload);

        out.println(jsobj.toString());
    }


}
