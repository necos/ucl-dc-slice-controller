package slice.console;

import slice.SliceController;
import cc.clayman.console.AbstractRestConsole;

/**
 * A Rest console for the SliceController
 */
public class SliceControllerConsole extends AbstractRestConsole {
    public SliceControllerConsole(SliceController controlller, int port) {
        setAssociated(controlller);
        initialise(port);
    }

    public void registerCommands() {

        // setup  /slice/ handler 
        defineRequestHandler("/slice/.*", new SliceHandler());

        // setup  /info/ handler 
        defineRequestHandler("/info/.*", new InfoHandler());

        // setup /command/ handler
        defineRequestHandler("/command/.*", new CommandHandler());


    }


    public SliceController getController() {
        return (SliceController)getAssociated();
    }
}
