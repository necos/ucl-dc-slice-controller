package slice;

import static us.monoid.web.Resty.content;
import static us.monoid.web.Resty.delete;
import static us.monoid.web.Resty.form;
import static us.monoid.web.Resty.put;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import us.monoid.web.Resty;
import cc.clayman.logging.Logger;
import cc.clayman.logging.MASK;

/**
 * Makes REST calls to SliceController using Resty
 */
public class SliceClient {

    // A URI for a SliceController to interact with
    String sliceMgrURI;
    Resty rest;
    int port;

    /**
     * Constructor for a SliceClient
     * to the ManagementConsole of a SliceController.
     * @param addr the name of the host
     * @param port the port the server is listening on
     */
    public SliceClient(String addr, int port) throws UnknownHostException, IOException  {
        initialize(InetAddress.getByName(addr), port);
    }

    /**
     * Constructor for a SliceClient
     * to the ManagementConsole of a SliceController.
     * @param addr the InetAddress of the host
     * @param port the port the server is listening on
     */
    public SliceClient(InetAddress addr, int port) throws UnknownHostException, IOException  {
        initialize(addr, port);
    }

    /**
     * Initialize
     */
    private synchronized void initialize(InetAddress addr, int port) {
        this.port = port;
        sliceMgrURI = "http://" + addr.getHostName() + ":" + Integer.toString(port);

        Logger.getLogger("log").logln(MASK.STDOUT, "sliceControllerURI: " + sliceMgrURI);

        rest = new Resty();
    }

    /**
     * Get the port this SliceClient is connecting to
     */
    public int getPort() {
        return port;
    }

    /**
     * Equivalent of: curl -X POST http://localhost:8090/slice/
     *
     * Returns JSONObject: {}
     */
    public JSONObject createSlice(String type, int hostCount) throws JSONException {
        try {
            String uri = sliceMgrURI + "/slice/";

            // adding form data causes a POST
            JSONObject jsobj = rest.json(uri, form("")).toObject();

            return jsobj;

        } catch (IOException ioe) {
            throw new JSONException("createSlice FAILED" + " IOException: " + ioe.getMessage());
        }
    }

    /**
     * Equivalent of: curl GET http://localhost:8090/slice/
     *
     * Returns JSONObject:  {"list":[12,6,5,7,8,9,10,1,3,11,4],"type":"slice"}
     */
    public JSONObject listSlices() throws JSONException {
        try {
            String uri = sliceMgrURI + "/slice/";

            JSONObject jsobj = rest.json(uri).toObject();

            return jsobj;

        } catch (IOException ioe) {
            throw new JSONException("listSlices FAILED" + " IOException: " + ioe.getMessage());
        }
    }
    
    /**
     * Equivalent of: curl GET http://localhost:8090/info/
     *
     * Returns JSONObject: 
     */
    public JSONObject getInfo() throws JSONException {
        try {
            String uri = sliceMgrURI + "/info/";

            JSONObject jsobj = rest.json(uri).toObject();

            return jsobj;

        } catch (IOException ioe) {
            throw new JSONException("info FAILED" + " IOException: " + ioe.getMessage());
        }
    }
    

}
